<?php
require_once "class_dbkoneksi.php";
class DAO // Data Access Object
{
    protected $tableName = "";
    protected $koneksi = null ;

    public function __construct($nama_table)
    {
        $this->tableName= $nama_table;
        $database = new DBKoneksi();
        $this->koneksi = $database->getKoneksi();
    }

    public function getAll(){
        $sql = "SELECT * FROM " . $this->tableName;
        $ps = $this->koneksi->prepare($sql);
        $ps->execute();
        return $ps->fetchAll();
    }

    public function hapus($pk){
        $sql = "DELETE FROM " . $this->tableName . " WHERE id=?";
        $ps = $this->koneksi->prepare($sql);
        $ps->execute([$pk]);
        return $ps->rowCount(); // jika 1 sukses
    }

    public function findByID($pk){
        $sql = "SELECT * FROM " . $this->tableName . " WHERE id=?";
        $ps = $this->koneksi->prepare($sql);
        $ps->execute([$pk]);
        return $ps->fetch();
    }

}
    //panggil file untuk koneksi ke DB
    //buat class
        //buat variabel yang akan digunakan
        //buat fungsi agar class ini nantinya bisa dipanggil ke class lain
        //buat fungsi dml yang akan digunakan dalam db
    //tutup class
?>
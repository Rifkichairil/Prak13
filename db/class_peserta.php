<?php
include_once 'top.php';
require_once 'class_dbkoneksi.php';

class Peserta {
 private $tableName = "peserta";
 private $koneksi = null ;
 public function __construct()
 {
 $database = new DBKoneksi();
 $this->koneksi = $database->getKoneksi();
 }
 public function getAll(){
 $sql = "SELECT * FROM " . $this->tableName;
 $ps = $this->koneksi->prepare($sql);
 $ps->execute();
 return $ps->fetchAll();
 }

 public function simpan($data){
 $sql = "INSERT INTO ".$this->tableName.
 " (id,nomor,email,namalengkap,hp,fbaccount,kegiatan_id,status,jenis_id,tgl_daftar) ".
 " VALUES (default,?,?,?,?,?,?,?,?,?)";
 $ps = $this->koneksi->prepare($sql);
 $ps->execute($data);
 return $ps->rowCount(); // jika 1 sukses
 }
 public function ubah($data){
 $sql = "UPDATE ".$this->tableName.
 " SET email=?,namalengkap=?,hp=?,fbaccount=?,kegiatn_id=?,status=?,jenis_id=?,tgl_daftar=? ". " WHERE ID=?";
 $ps = $this->koneksi->prepare($sql);
 $ps->execute($data);
 return $ps->rowCount(); // jika 1 sukses
 }
 public function hapus($pk){
 $sql = "DELETE FROM kegiatan WHERE id=?";
 $ps = $this->koneksi->prepare($sql);
 $ps->execute([$pk]);
 return $ps->rowCount(); // jika 1 sukses
 }
 public function findByID($pk){
 $sql = "SELECT * FROM " . $this->tableName . " WHERE id=?";
 $ps = $this->koneksi->prepare($sql);
 $ps->execute([$pk]);
 return $ps->fetch();
 }
}

    //masukkan deskripsi tabel agar mudah membuat fungsi. gunakan comment utk mempermudah
    //panggil file yang berisi semua fungsi dml yang bisa diakses oleh semua class
    //buat class dimana class ini merupakan turunan dari class yang dipanggil diatas
        //buat fungsi untuk bisa mengambil semua fungsi dari class parentnya
        //buat fungsi dml yang hanya bisa digunakan di class ini
    //tutup class
?>
<?php
include_once 'top.php';
require_once 'db/class_peserta.php';
?>

<h2>Daftar Peserta</h2>

<?php
 $obj_kegiatan = new Peserta();
 $rows = $obj_kegiatan->getAll();
?>

<table class="table">
 <thead>
 	<tr class="active">
		 <th>Id</th>
 		 <th>Nomor</th>
		 <th>Email</th>
		 <th>Nama Lengkap</th>
		 <th>No Hp</th>
		 <th>Fb Account</th>
		 <th>Kegiatan</th>
		 <th>Status</th>
		 <th>Jenis</th>
		 <th>Tanggal Daftar</th>
		 <th>Action</th>
 	</tr>
 </thead>
 <tbody>
 <?php
 $nomor = 1;
 foreach($rows as $row){
 echo '<tr><td>'.$nomor.'</td>';
 echo '<td>'.$row['nomor'].'</td>';
 echo '<td>'.$row['email'].'</td>';
 echo '<td>'.$row['namalengkap'].'</td>';
 echo '<td>'.$row['hp'].'</td>';
 echo '<td>'.$row['fbaccount'].'</td>';
 echo '<td>'.$row['kegiatan_id'].'</td>';
 echo '<td>'.$row['status'].'</td>';
 echo '<td>'.$row['jenis_id'].'</td>';
 echo '<td>'.$row['tgl_daftar'].'</td>';


 echo '<td><a href="view_kegiatan.php?id='.$row['id']. '">View</a> |';
 echo '<a href="form_kegiatan.php?id='.$row['id']. '">Update</a></td>';
 echo '</tr>';
 $nomor++;
 }
 ?>
 </tbody>
</table>
<?php
include_once 'bottom.php';

    //panggil template header
    //panggil class yang berisi fungsi dml
    //buat judul tabel
    //buat objek dimana dia akan memanggil fungsi dari class yang sudah dimasukkan
    //buat header untuk tampilan table
    //lakukan looping untuk menampilkan seluruh isi data table
    //tambahkan button untuk menambah data
    //panggil template footer
?>
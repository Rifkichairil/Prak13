<?php
	include_once 'top.php';
	require_once 'db/class_peserta.php';
?>

<?php
<form class="form-horizontal" method="POST" action="proses_kegiatan.php">
<form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Form Name</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email</label>  
  <div class="col-md-4">
  <input id="email" name="email" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="namalengkap">Nama Lengkap</label>  
  <div class="col-md-4">
  <input id="namalengkap" name="namalengkap" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="hp">No Hp</label>  
  <div class="col-md-4">
  <input id="hp" name="hp" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="fbaccount">Fb Account</label>  
  <div class="col-md-4">
  <input id="fbaccount" name="fbaccount" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="kegiatan_id">Kegiatan</label>
  <div class="col-md-4">
    <select id="kegiatan_id" name="kegiatan_id" class="form-control">
      <option value="1">Seminar Rembulan 1</option>
      <option value="2">Kuliah Umum</option>
      <option value="3">Seminar rembulan 2</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="status">Status</label>  
  <div class="col-md-4">
  <input id="status" name="status" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="jenis_id">Jenis</label>
  <div class="col-md-4">
    <select id="jenis_id" name="jenis_id" class="form-control">
      <option value="1">Option one</option>
      <option value="2">Option two</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tgl_daftar">Tanggal Daftar</label>  
  <div class="col-md-4">
  <input id="tgl_daftar" name="tgl_daftar" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Button (Double) -->
 <div class="form-group">
 <label class="col-md-4 control-label" for="proses"></label>
 <div class="col-md-8">
 <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
 <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
 <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
 </div>
 </div>
</fieldset>
</form>
?>

<?php
include_once 'bottom.php';
    //panggil template header
    //buat form untuk memasukkan input dimana saat melakukan input pertama kali, form akan kosong dan saat melakukan update, form sudah terisi dengan data yang akan diupdate
    //buat fungsi dimana button simpan hanya terlihat saat melakukan penambahan ke db dan fungsi update, delete hanya tampil saat melakukan update
    //panggil template footer
?>